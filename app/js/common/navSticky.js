/* Sticky nav */
(function() {
    const stickyContainer = document.querySelector('.js-sticky-nav');

    const sections = document.querySelectorAll('.js-observed-block')
    const pageHeader = document.querySelector('.page-header');

    let stickyHeight = null;
    let mainContainerRootMargin = null;
    let sectionObserverRootMargin = null;
    let isDesktop = false;
    let OFFSET = -10;

    let mainContainerObserver = null;
    let sectionObserver = null;

    function calcMargins() {
        stickyHeight = stickyContainer.offsetHeight;
        mainContainerRootMargin = -stickyHeight + OFFSET + "px 0px 0px 0px";
        sectionObserverRootMargin = -stickyHeight + OFFSET + "px 0px -50% 0px";
    }; 

    function setObservers() {
        calcMargins();

        mainContainerObserver = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    stickyContainer.classList.remove('sticky-nav--visible');
                } else {
                    stickyContainer.classList.add('sticky-nav--visible');
                }
            })
        }, {rootMargin: mainContainerRootMargin});
    
        sectionObserver = new IntersectionObserver((entries) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    const currentSection = entry.target;
                    const activeItem = stickyContainer.querySelector('.js-sticky-nav-item--current');
                    const currentItem = stickyContainer.querySelector('[data-target=' + currentSection.id + ']');
    
                    if (activeItem) {
                        activeItem.classList.remove('js-sticky-nav-item--current');
                    }
    
                    if (currentItem) {
                        currentItem.classList.add('js-sticky-nav-item--current');
                    }
    
                } else {
                    const currentItem = stickyContainer.querySelector('[data-target=' + entry.target.id + ']');
                    currentItem.classList.remove('js-sticky-nav-item--current');
                }
            })
        }, { rootMargin: sectionObserverRootMargin});
    }

    function initStickyNav() {
            setObservers();

            isDesktop = true;
            mainContainerObserver.observe(pageHeader);
            sections.forEach((section) => {
                sectionObserver.observe(section);
            })
    };


    function updateStickyNav() {
        if (window.matchMedia("(min-width: 768px)").matches) {
            isDesktop = true;
            setObservers();

            mainContainerObserver.observe(pageHeader);
            sections.forEach((section) => {
                sectionObserver.observe(section);
            })
        } else if (isDesktop && window.matchMedia("(max-width: 767px)").matches) {
            stickyContainer.classList.remove('sticky-nav--visible');
            mainContainerObserver.disconnect();
            sectionObserver.disconnect();
        }
    }

    if (window.matchMedia("(min-width: 767px)").matches) {
        document.addEventListener('DOMContentLoaded', initStickyNav);
    }
    
    window.addEventListener('resize', updateStickyNav);
})();