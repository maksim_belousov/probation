/*Lazy load*/

function initLazyLoad() {
    const images = document.querySelectorAll('img.lazyload')

    if ('loading' in HTMLImageElement.prototype) {
        images.forEach(img => {
            img.src = img.dataset.src;
        });
    } else {
        const imageObserver = new IntersectionObserver((entries, imgObserver) => {
            entries.forEach((entry) => {
                if (entry.isIntersecting) {
                    const lazyImage = entry.target
                    lazyImage.src = lazyImage.dataset.src
                    lazyImage.classList.remove("lazyload");
                    imgObserver.unobserve(lazyImage);
                }
            })
        });

        images.forEach((img) => {
            imageObserver.observe(img);
        })
    }
};