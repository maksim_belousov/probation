  /* Floating labels in request form*/

  (function() {
    const fields = document.querySelectorAll(".js-float-field");

    if (fields.length > 0) {
        floatLabels();
    }

    function floatLabels() {
        fields.forEach(function(field) {
            let input = field.querySelector('.js-float-input');
            let label = field.querySelector('.js-float-label');

            input.addEventListener("focus", function () {
                checkFocus(label);
            });
        
            input.addEventListener("blur", function () {
                checkBlur(input, label);
            });
        
            input.addEventListener("change keyup", function () {
                checkKeyup(input, label);
            });
        });
    }

    function checkFocus(label) {
        label.classList.add("float");
        label.classList.add("float--focus");
    }
  
    function checkBlur(input, label) {
        label.classList.remove("float--focus");

        window.setTimeout(() => {
            if (input.value === "") {
                label.classList.remove("float");
            }
        }, 300);
    }
    
    function checkKeyup(input, label) {
        if (input.value === "" && !input.focus()) {
            label.classList.remove("float");
        } else {
            label.classList.add("float");
        }
      }
  })();