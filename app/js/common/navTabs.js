/* Directions menu */

function switchDirections() {
    const navWrapper = document.querySelector('.js-tabs-nav-wrapper');
    const bodyWrapper = document.querySelector('.js-tabs-body-wrapper');
    const navs = navWrapper.querySelectorAll('.js-tabs-nav');
    const bodies = bodyWrapper.querySelectorAll('.js-tabs-body');

    navs.forEach((nav) => {
        nav.addEventListener('click', () => {
            navs.forEach((nav) => {
                nav.classList.remove('tabs-nav--active');
            });

            bodies.forEach((body) => {
                body.classList.remove('tabs-body--visible');

                if (body.dataset.tabName === nav.dataset.tabName) {
                    body.classList.add('tabs-body--visible');
                    nav.classList.add('tabs-nav--active');
                }
            });
        })
    });
};