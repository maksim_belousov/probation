/* Anchor links*/

(function() {
    const links = document.querySelectorAll('.js-anchor-link');
    const stickyNav = document.querySelector('.sticky-nav');
    const mainNav = document.querySelector('.main-nav');
    let offset = null;

    function getYOffset(el) {
        const rect = el.getBoundingClientRect();
        return rect.top + window.pageYOffset ;
    }

    function doScrolling(element, duration, offset) { 
        const elementY = getYOffset(element);
        const startingY = window.pageYOffset;
        const diff = elementY - startingY;
        let start;
      
        window.requestAnimationFrame(function step(timestamp) {
          if (!start) start = timestamp;
          const time = timestamp - start;
          const percent = Math.min(time / duration, 1);
      
          window.scrollTo(0, startingY + offset + diff * percent);
      
          if (time < duration) {
            window.requestAnimationFrame(step);
          }
        })
    }

    function calcOffset() {
        if (window.matchMedia('(min-width: 768px)').matches && stickyNav) {
            offset =  -stickyNav.offsetHeight;
        } else {
            offset = -mainNav.offsetHeight;
        }
    }

    function initAnchorLinks() {
        links.forEach((link) => {
            link.addEventListener('click', (evt) => {
                evt.preventDefault();

                calcOffset()
                
                let target = document.querySelector('#' + link.dataset.target);
                doScrolling(target, 400, offset);
            });
        });
    }

    function updateAnchorLinks() {
        calcOffset();
    }

    document.addEventListener('DOMContentLoaded', initAnchorLinks);
    window.addEventListener('resize', updateAnchorLinks);
})();