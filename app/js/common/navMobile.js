
/* Mobile menu */

(function() {
    const pageHeader = document.querySelector('.page-header');
    const teaserContainer = pageHeader.querySelector('.teaser');
    const barContainer = pageHeader.querySelector('.main-nav');
    const menuContainer = barContainer.querySelector('.main-nav__menu');
    const menuItems = barContainer.querySelectorAll('.js-nav-link');
    const menuToggleBtn = barContainer.querySelector('.js-mobile-menu-btn');
    let handlersAreReady = false;

    function setMenu() {
        const menuOffsetTop = barContainer.offsetHeight;
        menuContainer.style.paddingTop = menuOffsetTop + 'px';
        teaserContainer.style.paddingTop = menuOffsetTop + 'px';
    };

    function toggleMenuHandler(evt) {
        evt.preventDefault();
        menuContainer.classList.toggle('main-nav__menu--visible');
        menuToggleBtn.classList.toggle('opened');
        document.body.classList.toggle('main-nav-active');
    };

    function closeMenuHandler(evt) {
        evt.preventDefault();
        menuContainer.classList.remove('main-nav__menu--visible');
        menuToggleBtn.classList.remove('opened');
        document.body.classList.remove('main-nav-active');
    };

    function toggleMenu(evt, trigger) {
        trigger.addEventListener(evt, toggleMenuHandler);
    };

    function closeMenu(evt, trigger) {
        trigger.addEventListener(evt, closeMenuHandler);
    };
    
    function addHandlers() {
        toggleMenu('click', menuToggleBtn);

        menuItems.forEach((el) => {
            closeMenu('click', el);  
        });

        handlersAreReady = true;
    }

    function initMobileMenu() {
        setMenu();
        addHandlers();
    }

    function updateMobileMenu() {
        if (window.matchMedia("(max-width: 767px)").matches) {
            setMenu();

            if (!handlersAreReady) {
                addHandlers();
            }
        } else {
            menuContainer.classList.remove('main-nav__menu--visible');
            menuToggleBtn.classList.remove('opened');
            document.body.classList.remove('main-nav-active');
            teaserContainer.style.paddingTop = 0;
            menuContainer.style.paddingTop = 0;
        }
    };

    if (window.matchMedia("(max-width: 767px)").matches) {
        document.addEventListener('DOMContentLoaded', initMobileMenu);
    }

    window.addEventListener('resize', updateMobileMenu);
})();