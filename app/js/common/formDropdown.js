(() => {
    if (window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = Array.prototype.forEach;
    }

    const forms = document.querySelectorAll('form');
    let inputs = null;

    forms.forEach((form) => {
        inputs = form.querySelectorAll('.js-acomplete-input');
    });

    const url = 'russia.json';
    const method = 'GET';
    const arrays = {
        cities: [],
    }

    const MENU_ITEMS_QUANTITY = 6;

    let closeHandler = false;

    const xhr = new XMLHttpRequest();
    
    xhr.addEventListener('load', function () {
        const serverResponse = JSON.parse(xhr.responseText);

        getCities(serverResponse);

        inputs.forEach((input) => {
            if (input.dataset.acompleteList) {
                autocomplete(input, input.dataset.acompleteList.split(','));
            } else if (input.dataset.acompleteArray) {
                autocomplete(input, arrays[input.dataset.acompleteArray]);
            } else {
                console.warn(`список для меню input'a ${input.id} не найден`);
            }
        });
    });
    
    xhr.open(method, url);
    xhr.send();

    function getCities(array) {
        array.forEach((el) => {
            arrays.cities.push(el.city);
        })
    }

    function autocomplete(input, array) {
        let currentFocus;

        input.addEventListener('click', function(event) {
            const inputValue = this.value;

            if (!input.parentNode.querySelector('.autocomplete-list')) {

                createDropdownList(array, inputValue);

            }
        });

        input.addEventListener('input', function(event) {
            const inputValue = this.value;

            closeAllLists();

            currentFocus = -1;

            createDropdownList(array, inputValue, true)
                
        });

        input.addEventListener('keydown', function(event) {
            let x = document.getElementById(this.id + 'autocomplete-list');

            if (x) {
                x = x.getElementsByTagName('div');
            }

            if (event.keyCode == 13) { //enter
                event.preventDefault();
                if (currentFocus > -1) {
                    if (x) x[currentFocus].click();
                }
            }
        });

        function createDropdownList(array, inputValue, typing) {

            const dropdownList = document.createElement('DIV');
            let i = 0;

            dropdownList.setAttribute('id', input.id + '-autocomplete-list');
            dropdownList.classList.add('autocomplete-list'); 
            input.parentNode.appendChild(dropdownList);

            input.parentNode.classList.add('dropdown-active');

            array.forEach((item, index) => {
                    if (i < MENU_ITEMS_QUANTITY && item.substr(0, inputValue.length).toUpperCase() == inputValue.toUpperCase()) {
                        const dropdownItem = document.createElement('DIV');
                        dropdownItem.classList.add('autocomplete-item');
                        dropdownItem.insertAdjacentHTML('afterbegin', '<strong>' + item.substr(0, inputValue.length) + '</strong>');
                        dropdownItem.insertAdjacentHTML('beforeend', item.substr(inputValue.length));
                        dropdownItem.insertAdjacentHTML('beforeend', '<input type="hidden" value="' + item + '">');
                        dropdownList.appendChild(dropdownItem);

                        dropdownItem.addEventListener('click', function(e) {
                            input.value = this.getElementsByTagName('input')[0].value;
                            closeAllLists();
                        });
                        i++;
                    }
            });
        }

        function closeAllLists(element) {
            var x = document.querySelectorAll('.autocomplete-list');

            x.forEach((item) => {
                if (element != item && element != item.parentNode.querySelector('input')) {
                    item.parentNode.classList.remove('dropdown-active');
                    item.parentNode.removeChild(item);
                }
            });
        }

        if (!closeHandler) {
            document.addEventListener('click', closeListsHandler);
            closeHandler = true;
        }

        function closeListsHandler(event) {
            closeAllLists(event.target);
        }
    }

})();