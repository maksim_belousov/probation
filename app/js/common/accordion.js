/* Accordion */

function animateAccordion() {
    const accordion = document.querySelectorAll(".js-accordion");

    accordion.forEach((el) => {
        let control = el.querySelector('.js-accordion-control'); 

        control.addEventListener('click', function() {
            el.classList.toggle('active');

            let panel = this.nextElementSibling;

            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
              } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
              }
        });
    });
}