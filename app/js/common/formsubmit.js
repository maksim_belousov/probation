function submitForm(submit) {
    const url = 'russia.json';
    const method = 'POST';

    const modalId = submit.dataset.modalId;
    submit.disabled = true;
    
        var xhr = new XMLHttpRequest();

        xhr.responseType = 'json';
        
        xhr.addEventListener('load', function () {
            openModal(modalId);
            submit.disabled = false;
        });
        
        xhr.open(method, url);
        xhr.send();
}
