/* Device detecting */

function detectDevice() {
    let deviceOs = getMobileOs();
    document.body.classList.add('platform_' + deviceOs);
}

function getMobileOs(){
    let userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if(/android/i.test(userAgent)) {return 'android'}
    if(/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream){return 'ios'}
    return 'unknown'
}