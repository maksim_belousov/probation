'use strict';

/* Accordion */

function animateAccordion() {
    var accordion = document.querySelectorAll(".js-accordion");

    accordion.forEach(function (el) {
        var control = el.querySelector('.js-accordion-control');

        control.addEventListener('click', function () {
            el.classList.toggle('active');

            var panel = this.nextElementSibling;

            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            }
        });
    });
}
'use strict';

/* Device detecting */

function detectDevice() {
    var deviceOs = getMobileOs();
    document.body.classList.add('platform_' + deviceOs);
}

function getMobileOs() {
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (/android/i.test(userAgent)) {
        return 'android';
    }
    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
        return 'ios';
    }
    return 'unknown';
}
'use strict';

/* forEach polyfill*/

/* Object.assign polyfill*/

// if (typeof Object.assign != 'function') {
//     Object.assign = function(target) {
//       'use strict';
//       if (target == null) {
//         throw new TypeError('Cannot convert undefined or null to object');
//       }

//       target = Object(target);
//       for (var index = 1; index < arguments.length; index++) {
//         var source = arguments[index];
//         if (source != null) {
//           for (var key in source) {
//             if (Object.prototype.hasOwnProperty.call(source, key)) {
//               target[key] = source[key];
//             }
//           }
//         }
//       }
//       return target;
//     };
//   }

(function () {
    function initCommonScripts() {
        detectDevice();
        switchDirections();
        animateAccordion();

        svg4everybody({
            polyfill: true // polyfill <use> elements for External Content
        });

        new WOW().init();
    };

    document.addEventListener('DOMContentLoaded', initCommonScripts);
})();
'use strict';

(function () {
    if (window.NodeList && !NodeList.prototype.forEach) {
        NodeList.prototype.forEach = Array.prototype.forEach;
    }

    var forms = document.querySelectorAll('form');
    var inputs = null;

    forms.forEach(function (form) {
        inputs = form.querySelectorAll('.js-acomplete-input');
    });

    var url = 'russia.json';
    var method = 'GET';
    var arrays = {
        cities: []
    };

    var MENU_ITEMS_QUANTITY = 6;

    var closeHandler = false;

    var xhr = new XMLHttpRequest();

    xhr.addEventListener('load', function () {
        var serverResponse = JSON.parse(xhr.responseText);

        getCities(serverResponse);

        inputs.forEach(function (input) {
            if (input.dataset.acompleteList) {
                autocomplete(input, input.dataset.acompleteList.split(','));
            } else if (input.dataset.acompleteArray) {
                autocomplete(input, arrays[input.dataset.acompleteArray]);
            } else {
                console.warn('\u0441\u043F\u0438\u0441\u043E\u043A \u0434\u043B\u044F \u043C\u0435\u043D\u044E input\'a ' + input.id + ' \u043D\u0435 \u043D\u0430\u0439\u0434\u0435\u043D');
            }
        });
    });

    xhr.open(method, url);
    xhr.send();

    function getCities(array) {
        array.forEach(function (el) {
            arrays.cities.push(el.city);
        });
    }

    function autocomplete(input, array) {
        var currentFocus = void 0;

        input.addEventListener('click', function (event) {
            var inputValue = this.value;

            if (!input.parentNode.querySelector('.autocomplete-list')) {

                createDropdownList(array, inputValue);
            }
        });

        input.addEventListener('input', function (event) {
            var inputValue = this.value;

            closeAllLists();

            currentFocus = -1;

            createDropdownList(array, inputValue, true);
        });

        input.addEventListener('keydown', function (event) {
            var x = document.getElementById(this.id + 'autocomplete-list');

            if (x) {
                x = x.getElementsByTagName('div');
            }

            if (event.keyCode == 13) {
                //enter
                event.preventDefault();
                if (currentFocus > -1) {
                    if (x) x[currentFocus].click();
                }
            }
        });

        function createDropdownList(array, inputValue, typing) {

            var dropdownList = document.createElement('DIV');
            var i = 0;

            dropdownList.setAttribute('id', input.id + '-autocomplete-list');
            dropdownList.classList.add('autocomplete-list');
            input.parentNode.appendChild(dropdownList);

            input.parentNode.classList.add('dropdown-active');

            array.forEach(function (item, index) {
                if (i < MENU_ITEMS_QUANTITY && item.substr(0, inputValue.length).toUpperCase() == inputValue.toUpperCase()) {
                    var dropdownItem = document.createElement('DIV');
                    dropdownItem.classList.add('autocomplete-item');
                    dropdownItem.insertAdjacentHTML('afterbegin', '<strong>' + item.substr(0, inputValue.length) + '</strong>');
                    dropdownItem.insertAdjacentHTML('beforeend', item.substr(inputValue.length));
                    dropdownItem.insertAdjacentHTML('beforeend', '<input type="hidden" value="' + item + '">');
                    dropdownList.appendChild(dropdownItem);

                    dropdownItem.addEventListener('click', function (e) {
                        input.value = this.getElementsByTagName('input')[0].value;
                        closeAllLists();
                    });
                    i++;
                }
            });
        }

        function closeAllLists(element) {
            var x = document.querySelectorAll('.autocomplete-list');

            x.forEach(function (item) {
                if (element != item && element != item.parentNode.querySelector('input')) {
                    item.parentNode.classList.remove('dropdown-active');
                    item.parentNode.removeChild(item);
                }
            });
        }

        if (!closeHandler) {
            document.addEventListener('click', closeListsHandler);
            closeHandler = true;
        }

        function closeListsHandler(event) {
            closeAllLists(event.target);
        }
    }
})();
'use strict';

/* Floating labels in request form*/

(function () {
    var fields = document.querySelectorAll(".js-float-field");

    if (fields.length > 0) {
        floatLabels();
    }

    function floatLabels() {
        fields.forEach(function (field) {
            var input = field.querySelector('.js-float-input');
            var label = field.querySelector('.js-float-label');

            input.addEventListener("focus", function () {
                checkFocus(label);
            });

            input.addEventListener("blur", function () {
                checkBlur(input, label);
            });

            input.addEventListener("change keyup", function () {
                checkKeyup(input, label);
            });
        });
    }

    function checkFocus(label) {
        label.classList.add("float");
        label.classList.add("float--focus");
    }

    function checkBlur(input, label) {
        label.classList.remove("float--focus");

        window.setTimeout(function () {
            if (input.value === "") {
                label.classList.remove("float");
            }
        }, 300);
    }

    function checkKeyup(input, label) {
        if (input.value === "" && !input.focus()) {
            label.classList.remove("float");
        } else {
            label.classList.add("float");
        }
    }
})();
"use strict";

window.addEventListener("DOMContentLoaded", function () {

    function setCursorPosition(pos, elem) {
        elem.focus();
        if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);else if (elem.createTextRange) {
            var range = elem.createTextRange();
            range.collapse(true);
            range.moveEnd("character", pos);
            range.moveStart("character", pos);
            range.select();
        }
    }

    function mask(event) {
        var matrix = "+7 (___) ___-__-__",
            i = 0,
            def = matrix.replace(/\D/g, ""),
            val = this.value.replace(/\D/g, "");
        if (def.length >= val.length) val = def;
        this.value = matrix.replace(/./g, function (a) {
            return (/[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
            );
        });
        if (event.type == "blur") {
            if (this.value.length == 2) this.value = "";
        } else setCursorPosition(this.value.length, this);
    };

    var input = document.querySelector("#phoneField");
    input.addEventListener("input", mask, false);
    input.addEventListener("focus", mask, false);
    input.addEventListener("blur", mask, false);
});
'use strict';

function submitForm(submit) {
    var url = 'russia.json';
    var method = 'POST';

    var modalId = submit.dataset.modalId;
    submit.disabled = true;

    var xhr = new XMLHttpRequest();

    xhr.responseType = 'json';

    xhr.addEventListener('load', function () {
        openModal(modalId);
        submit.disabled = false;
    });

    xhr.open(method, url);
    xhr.send();
}
'use strict';

/* Textarea autoresize with typing*/

document.addEventListener('DOMContentLoaded', function () {

  var textareas = document.querySelectorAll('.js-textarea-autoresize');
  var INITIAL_HEIGHT = 5;

  var observeEvent = function observeEvent(element, event, handler) {
    element.addEventListener(event, function () {
      handler(element);
    });
  };

  function resize(el) {
    el.style.height = el.scrollHeight + 'px';
  }

  function delayedResize(input) {
    window.setTimeout(function () {
      resize(input);
    }, 0);
  }

  function initTextareaAutoresize() {

    textareas.forEach(function (input) {
      observeEvent(input, 'change', resize);
      observeEvent(input, 'cut', delayedResize);
      observeEvent(input, 'paste', delayedResize);
      observeEvent(input, 'drop', delayedResize);
      observeEvent(input, 'keydown', delayedResize);

      resize(input);
    });
  }

  if (textareas.length > 0) {
    initTextareaAutoresize();
  };
});
'use strict';

var form = document.querySelector('.request__form');
var inputs = document.querySelectorAll('input');
var submit = form.querySelector('.request__btn');
var isInvalid = false;

function checkElementValidity(el, inputIsBlurred) {
    var errorList = el.parentNode.querySelector('.error-list');

    if (errorList) {
        el.parentNode.removeChild(errorList);
        el.classList.remove('is-invalid');
    }

    if (el.checkValidity() == false) {

        var inputCustomValidation = new CustomValidation();
        inputCustomValidation.checkValidity(el, inputIsBlurred);
        var customValidityMessage = inputCustomValidation.getInvalidities();
        el.setCustomValidity(customValidityMessage);

        var customValidityMessageForHTML = inputCustomValidation.getInvaliditiesForHTML();

        if (customValidityMessageForHTML.length > 0) {
            el.insertAdjacentHTML('afterend', '<div class="error-list"></div>');
            el.parentNode.querySelector('.error-list').insertAdjacentHTML('afterbegin', '<span class="error-text"><span>' + customValidityMessageForHTML + '</span></span>');
            el.classList.add('is-invalid');
        }

        isInvalid = true;
    }
}

inputs.forEach(function (input) {
    input.addEventListener('blur', function (event) {
        var _this = this;

        window.setTimeout(function () {
            checkElementValidity(_this, true);
        }, 100);
    });

    input.addEventListener('input', function (event) {
        checkElementValidity(this, true);
    });
});

submit.addEventListener('click', function (e) {
    isInvalid = false;

    inputs.forEach(function (input) {
        checkElementValidity(input);
    });

    if (!isInvalid) {
        submitForm(this);
    }
});
'use strict';

/*Lazy load*/

function initLazyLoad() {
    var images = document.querySelectorAll('img.lazyload');

    if ('loading' in HTMLImageElement.prototype) {
        images.forEach(function (img) {
            img.src = img.dataset.src;
        });
    } else {
        var imageObserver = new IntersectionObserver(function (entries, imgObserver) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var lazyImage = entry.target;
                    lazyImage.src = lazyImage.dataset.src;
                    lazyImage.classList.remove("lazyload");
                    imgObserver.unobserve(lazyImage);
                }
            });
        });

        images.forEach(function (img) {
            imageObserver.observe(img);
        });
    }
};
'use strict';

var modalStates = {
    hidden: 'popup-modal-hidden',
    active: 'modal-box-active',
    ready: 'modal-box-ready',
    noscroll: 'modal-box-viewed'
};

var animateEnd = 'webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend';
var modalOpenTriggers = document.querySelectorAll('.js-show-modal');

modalOpenTriggers.forEach(function (el) {
    el.addEventListener('click', function (evt) {
        evt.preventDefault();
        evt.stopPropagation();

        var modalId = this.dataset.modalId;

        openModal(modalId);
    });
});

function openModal(id) {

    var modal = document.querySelector('' + id);

    if (modal) {
        var closeTriggerHandler = function closeTriggerHandler(evt) {
            evt.preventDefault();

            closeModal(modal);
        };

        var outsideClickHandler = function outsideClickHandler(evt) {
            if (!modal.contains(evt.target)) {
                closeModal(modal);
            }
        };

        var escKeydownHandler = function escKeydownHandler(evt) {
            if (evt.key === 'Escape') {
                closeModal(modal);
            }
        };

        var closeModal = function closeModal(el) {
            document.body.classList.remove(modalStates.noscroll);
            el.parentNode.classList.add(modalStates.hidden);
            el.classList.remove(modalStates.active);
            el.classList.remove(modalStates.ready);

            modalCloseTriggers.forEach(function (el) {
                el.removeEventListener('click', closeTriggerHandler);
            });
            document.removeEventListener('click', outsideClickHandler);
            window.removeEventListener('keydown', escKeydownHandler);
        };

        var modalWrapper = modal.parentNode;

        modal.classList.add(modalStates.active);

        document.body.classList.add(modalStates.noscroll);

        modalWrapper.classList.remove(modalStates.hidden);

        modalWrapper.addEventListener(animateEnd, function (evt) {
            modal.classList.add(modalStates.ready);
        });

        var modalCloseTriggers = modal.querySelectorAll('.js-close-modal');

        modalCloseTriggers.forEach(function (el) {
            el.addEventListener('click', closeTriggerHandler);
        });

        document.addEventListener('click', outsideClickHandler);

        window.addEventListener('keydown', escKeydownHandler);
    } else {
        console.log('modal id is not found');
    }
}
'use strict';

/* Anchor links*/

(function () {
    var links = document.querySelectorAll('.js-anchor-link');
    var stickyNav = document.querySelector('.sticky-nav');
    var mainNav = document.querySelector('.main-nav');
    var offset = null;

    function getYOffset(el) {
        var rect = el.getBoundingClientRect();
        return rect.top + window.pageYOffset;
    }

    function doScrolling(element, duration, offset) {
        var elementY = getYOffset(element);
        var startingY = window.pageYOffset;
        var diff = elementY - startingY;
        var start = void 0;

        window.requestAnimationFrame(function step(timestamp) {
            if (!start) start = timestamp;
            var time = timestamp - start;
            var percent = Math.min(time / duration, 1);

            window.scrollTo(0, startingY + offset + diff * percent);

            if (time < duration) {
                window.requestAnimationFrame(step);
            }
        });
    }

    function calcOffset() {
        if (window.matchMedia('(min-width: 768px)').matches && stickyNav) {
            offset = -stickyNav.offsetHeight;
        } else {
            offset = -mainNav.offsetHeight;
        }
    }

    function initAnchorLinks() {
        links.forEach(function (link) {
            link.addEventListener('click', function (evt) {
                evt.preventDefault();

                calcOffset();

                var target = document.querySelector('#' + link.dataset.target);
                doScrolling(target, 400, offset);
            });
        });
    }

    function updateAnchorLinks() {
        calcOffset();
    }

    document.addEventListener('DOMContentLoaded', initAnchorLinks);
    window.addEventListener('resize', updateAnchorLinks);
})();
'use strict';

/* Mobile menu */

(function () {
    var pageHeader = document.querySelector('.page-header');
    var teaserContainer = pageHeader.querySelector('.teaser');
    var barContainer = pageHeader.querySelector('.main-nav');
    var menuContainer = barContainer.querySelector('.main-nav__menu');
    var menuItems = barContainer.querySelectorAll('.js-nav-link');
    var menuToggleBtn = barContainer.querySelector('.js-mobile-menu-btn');
    var handlersAreReady = false;

    function setMenu() {
        var menuOffsetTop = barContainer.offsetHeight;
        menuContainer.style.paddingTop = menuOffsetTop + 'px';
        teaserContainer.style.paddingTop = menuOffsetTop + 'px';
    };

    function toggleMenuHandler(evt) {
        evt.preventDefault();
        menuContainer.classList.toggle('main-nav__menu--visible');
        menuToggleBtn.classList.toggle('opened');
        document.body.classList.toggle('main-nav-active');
    };

    function closeMenuHandler(evt) {
        evt.preventDefault();
        menuContainer.classList.remove('main-nav__menu--visible');
        menuToggleBtn.classList.remove('opened');
        document.body.classList.remove('main-nav-active');
    };

    function toggleMenu(evt, trigger) {
        trigger.addEventListener(evt, toggleMenuHandler);
    };

    function closeMenu(evt, trigger) {
        trigger.addEventListener(evt, closeMenuHandler);
    };

    function addHandlers() {
        toggleMenu('click', menuToggleBtn);

        menuItems.forEach(function (el) {
            closeMenu('click', el);
        });

        handlersAreReady = true;
    }

    function initMobileMenu() {
        setMenu();
        addHandlers();
    }

    function updateMobileMenu() {
        if (window.matchMedia("(max-width: 767px)").matches) {
            setMenu();

            if (!handlersAreReady) {
                addHandlers();
            }
        } else {
            menuContainer.classList.remove('main-nav__menu--visible');
            menuToggleBtn.classList.remove('opened');
            document.body.classList.remove('main-nav-active');
            teaserContainer.style.paddingTop = 0;
            menuContainer.style.paddingTop = 0;
        }
    };

    if (window.matchMedia("(max-width: 767px)").matches) {
        document.addEventListener('DOMContentLoaded', initMobileMenu);
    }

    window.addEventListener('resize', updateMobileMenu);
})();
'use strict';

/* Sticky nav */
(function () {
    var stickyContainer = document.querySelector('.js-sticky-nav');

    var sections = document.querySelectorAll('.js-observed-block');
    var pageHeader = document.querySelector('.page-header');

    var stickyHeight = null;
    var mainContainerRootMargin = null;
    var sectionObserverRootMargin = null;
    var isDesktop = false;
    var OFFSET = -10;

    var mainContainerObserver = null;
    var sectionObserver = null;

    function calcMargins() {
        stickyHeight = stickyContainer.offsetHeight;
        mainContainerRootMargin = -stickyHeight + OFFSET + "px 0px 0px 0px";
        sectionObserverRootMargin = -stickyHeight + OFFSET + "px 0px -50% 0px";
    };

    function setObservers() {
        calcMargins();

        mainContainerObserver = new IntersectionObserver(function (entries) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    stickyContainer.classList.remove('sticky-nav--visible');
                } else {
                    stickyContainer.classList.add('sticky-nav--visible');
                }
            });
        }, { rootMargin: mainContainerRootMargin });

        sectionObserver = new IntersectionObserver(function (entries) {
            entries.forEach(function (entry) {
                if (entry.isIntersecting) {
                    var currentSection = entry.target;
                    var activeItem = stickyContainer.querySelector('.js-sticky-nav-item--current');
                    var currentItem = stickyContainer.querySelector('[data-target=' + currentSection.id + ']');

                    if (activeItem) {
                        activeItem.classList.remove('js-sticky-nav-item--current');
                    }

                    if (currentItem) {
                        currentItem.classList.add('js-sticky-nav-item--current');
                    }
                } else {
                    var _currentItem = stickyContainer.querySelector('[data-target=' + entry.target.id + ']');
                    _currentItem.classList.remove('js-sticky-nav-item--current');
                }
            });
        }, { rootMargin: sectionObserverRootMargin });
    }

    function initStickyNav() {
        setObservers();

        isDesktop = true;
        mainContainerObserver.observe(pageHeader);
        sections.forEach(function (section) {
            sectionObserver.observe(section);
        });
    };

    function updateStickyNav() {
        if (window.matchMedia("(min-width: 768px)").matches) {
            isDesktop = true;
            setObservers();

            mainContainerObserver.observe(pageHeader);
            sections.forEach(function (section) {
                sectionObserver.observe(section);
            });
        } else if (isDesktop && window.matchMedia("(max-width: 767px)").matches) {
            stickyContainer.classList.remove('sticky-nav--visible');
            mainContainerObserver.disconnect();
            sectionObserver.disconnect();
        }
    }

    if (window.matchMedia("(min-width: 767px)").matches) {
        document.addEventListener('DOMContentLoaded', initStickyNav);
    }

    window.addEventListener('resize', updateStickyNav);
})();
'use strict';

/* Directions menu */

function switchDirections() {
    var navWrapper = document.querySelector('.js-tabs-nav-wrapper');
    var bodyWrapper = document.querySelector('.js-tabs-body-wrapper');
    var navs = navWrapper.querySelectorAll('.js-tabs-nav');
    var bodies = bodyWrapper.querySelectorAll('.js-tabs-body');

    navs.forEach(function (nav) {
        nav.addEventListener('click', function () {
            navs.forEach(function (nav) {
                nav.classList.remove('tabs-nav--active');
            });

            bodies.forEach(function (body) {
                body.classList.remove('tabs-body--visible');

                if (body.dataset.tabName === nav.dataset.tabName) {
                    body.classList.add('tabs-body--visible');
                    nav.classList.add('tabs-nav--active');
                }
            });
        });
    });
};
'use strict';

/* Swiper */

var swiper = new Swiper('.swiper-container', {
    pagination: {
        el: '.swiper-pagination',
        type: 'bullets'
    },
    navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
    },

    spaceBetween: 29,

    breakpoints: {
        768: {
            spaceBetween: 5
        }
    }
});
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var CustomValidation = function () {
  function CustomValidation() {
    _classCallCheck(this, CustomValidation);

    this.invalidities = [];
  }

  _createClass(CustomValidation, [{
    key: 'checkValidity',
    value: function checkValidity(input) {
      var inputIsBlurred = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;


      var validity = input.validity;

      if (validity.patternMismatch) {
        this.addInvalidity(input.dataset.errorMessagePattern);
      }

      if (validity.tooLong) {
        var maxlength = input.getAttribute('maxlength');
        this.addInvalidity('Количество символов не должно быть больше ' + maxlength);
      }

      if (validity.tooShort) {
        var minlength = input.getAttribute('minlength');
        this.addInvalidity('Количество символов не должно быть меньше ' + minlength);
      }

      if (validity.typeMismatch) {
        var type = input.getAttribute('type');
        this.addInvalidity('Неверный формат');
      }

      if (validity.valueMissing && !inputIsBlurred) {
        this.addInvalidity('Поле необходимо заполнить');
      }
    }
  }, {
    key: 'addInvalidity',
    value: function addInvalidity(message) {
      this.invalidities.push(message);
    }
  }, {
    key: 'getInvalidities',
    value: function getInvalidities() {
      return this.invalidities.join('. \n');
    }
  }, {
    key: 'getInvaliditiesForHTML',
    value: function getInvaliditiesForHTML() {
      return this.invalidities.join('. <br>');
    }
  }]);

  return CustomValidation;
}();

;
"use strict";

/* forEach polyfill*/

if (window.NodeList && !NodeList.prototype.forEach) {
    NodeList.prototype.forEach = Array.prototype.forEach;
}